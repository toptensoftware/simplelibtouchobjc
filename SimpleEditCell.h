//
//  SimpleEditCell.h
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimpleTableViewController.h"
#import "SimpleTableViewCell.h"

@interface SimpleEditCell : SimpleTableViewCell<HasValue, SelectHandler, UITextFieldDelegate, FirstResponder, MeasureHeight, HasAlignment> {

	UITextField*	m_editor;
	int				m_alignment;
}

- (id)initWithLabel:(NSString*)labelText;
- (id)initWithLabel:(NSString*)labelText andText:(NSString*)text;
- (id)initWithLabel:(NSString*)labelText andText:(NSString*)text andPlaceholder:(NSString*)placeholder;
- (id)initWithPlaceholder:(NSString*)placeholder;
- (id)initWithPlaceholder:(NSString*)placeholder andText:(NSString*)text;
- (UITextField*)editor;

@property (nonatomic, assign) BOOL showKeyboardNavBar;
@property (nonatomic, copy) void (^ChangeTextBlock)(NSString* value);
@property (nonatomic, strong) NSString* TextValue;
@property (nonatomic, strong) NSString* HintText;


@end
