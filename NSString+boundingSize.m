//
//  NSString+boundingSize.m
//  SimpleLibTouch
//
//  Created by Brad Robinson on 16/10/13.
//
//

#import "NSString+boundingSize.h"

@implementation NSString (boundingSize)

- (CGSize)boundingSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode
{
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    
    NSStringDrawingOptions options = NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin;
    
    NSDictionary* attributes =
                @{
                NSFontAttributeName : font,
                NSParagraphStyleAttributeName : paragraphStyle
                };
    
    CGSize retv = [self boundingRectWithSize:size options:options attributes:attributes context:nil].size;
    
    return CGSizeMake(ceilf(retv.width), ceilf(retv.height));
}

- (CGSize)boundingSizeWithFont:(UIFont *)font
{
    return [self sizeWithAttributes:@{NSFontAttributeName:font}];
}


@end
