//
//  SimpleDisclosureCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleDisclosureCell.h"
#import "SimpleAppearance.h"
#import "NSString+boundingSize.h"

@interface SimpleDisclosureCell()
{
    UIViewController* _viewController;
    UILabel* _valueLabel;
    CGFloat _alignment;
}
@end

@implementation SimpleDisclosureCell

@synthesize ViewControllerClass;
@synthesize ReuseViewController;
@synthesize MultiLine;
@synthesize SelectedBlock;

- (void)setupAppearance
{
    // Setup appearance
    SimpleAppearance* a = [SimpleAppearance Instance];
    if (a!=nil)
    {
        self.textLabel.font = a.labelFont;
        self.textLabel.textColor = a.labelColor;
        self.detailTextLabel.font = a.hintFont;
        self.detailTextLabel.textColor = a.hintColor;
    }

}


- (id)initWithText:(NSString*)text
{
	if (!(self=[super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil]))
		return nil;
	
	self.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    self.textLabel.text = text;
    self.detailTextLabel.Text = @"";

    [self setupAppearance];

    _alignment = 0;

	return self;
}


- (BOOL)didSelect:(UIViewController*)viewController
{
    if (self.SelectedBlock!=nil)
    {
        self.SelectedBlock();
    }
	else if (self.ViewControllerClass!=nil)
	{
        // Reuse previous view controller, or create a new one
        UIViewController* pViewController = _viewController;
        if (pViewController==nil)
        {
            pViewController=[self.ViewControllerClass new];
        }
        
        if (pViewController!=nil)
        {
            // Show it
            [viewController.navigationController pushViewController:pViewController animated:YES];
            
            // Store view controller for next time (if reuse enabled)
            if (self.ReuseViewController && _viewController==nil)
                _viewController = pViewController;
        }
	}

	return YES;
}

- (BOOL)shouldResignFirstResponder
{
	return TRUE;
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    if (MultiLine)
    {
        self.textLabel.numberOfLines = 0;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        CGSize size = [self.textLabel.text boundingSizeWithFont:self.textLabel.font constrainedToSize:CGSizeMake(self.bounds.size.width-50, 10000) lineBreakMode:NSLineBreakByWordWrapping];
        self.textLabel.frame = CGRectMake(20, 10, size.width, size.height);
    }
    else if (_valueLabel!=nil)
    {
        [self.textLabel sizeToFit];

        float xPos;
        if (_alignment!=0)
            xPos = _alignment;
        else
            xPos = self.textLabel.frame.origin.x + self.textLabel.frame.size.width + 5;

        if (self.detailTextLabel.text.length>0)
        {
            float xPosDetail = self.detailTextLabel.frame.origin.x + self.detailTextLabel.frame.size.width + 5;
            if (xPosDetail > xPos)
                xPos = xPosDetail;
        }


        _valueLabel.frame = CGRectMake(xPos,
                0,
                self.superview.frame.size.width - xPos - 50, self.contentView.bounds.size.height);
    }
}

- (CGFloat)measureHeightForWidth:(CGFloat)width
{
    if (MultiLine)
    {
        CGSize size = [self.textLabel.text boundingSizeWithFont:self.textLabel.font constrainedToSize:CGSizeMake(width-50, 10000) lineBreakMode:NSLineBreakByWordWrapping];
        return size.height + 20;
    }
    else
        return 44;
}


- (UIAccessibilityTraits)accessibilityTraits
{
    return UIAccessibilityTraitButton;
}

- (NSString*) Label
{
    return self.textLabel.text;
}

- (void) setLabel:(NSString*)value
{
    self.textLabel.text = value;
}

- (NSString*) Value
{
    if (_valueLabel==nil)
        return nil;

    return _valueLabel.text;
}

- (void) setValue:(NSString*) value
{
    if (_valueLabel==nil)
    {
        // Create the value label
        _valueLabel = [[UILabel alloc] init];
        _valueLabel.backgroundColor = [UIColor clearColor];
        _valueLabel.textAlignment = self.AlignValue ? NSTextAlignmentLeft : NSTextAlignmentRight;
        _valueLabel.numberOfLines = 1;
        _valueLabel.adjustsFontSizeToFitWidth = NO;
        _valueLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _valueLabel.highlightedTextColor = self.detailTextLabel.highlightedTextColor;

        SimpleAppearance* a = [SimpleAppearance Instance];
        if (a!=nil)
        {
            _valueLabel.font = a.valueFont;
            _valueLabel.textColor = a.valueColor;
        }

        [self.contentView addSubview:_valueLabel];
        [self setNeedsLayout];
    }
    _valueLabel.text = value;
}

- (NSString*) SubText
{
    return self.detailTextLabel.text;
}

- (void) setSubText:(NSString*) value
{
    self.detailTextLabel.Text = value;
}

- (void)setValue:(id)value animate:(BOOL)animate
{
    if ([value isKindOfClass:[NSString class]])
        self.Value=(NSString*)value;
}

- (id)getValue
{
    return _valueLabel.text;
}


- (CGFloat)calcPreferredAlignment
{
    if (self.AlignValue)
    {
        if (self.textLabel.text.length == 0)
            return 0;
        [self.textLabel sizeToFit];
        return self.textLabel.frame.size.width + 20;
    }
    else
    {
        return 0;
    }
}
- (void)setActualAlignment:(CGFloat)alignment
{
    if (self.AlignValue)
    {
        _alignment = alignment;
        [self setNeedsLayout];
    }
}

@end/* This custom view behaves like a button. */

