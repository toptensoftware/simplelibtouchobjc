//
// Created by Brad Robinson on 30/05/13.


#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "SimpleTableViewController.h"

@class SimpleTableViewController;
@class UIView;
@class UITableViewCell;


@interface SimpleTableViewSection : NSObject<HasAlignment>

@property (nonatomic, unsafe_unretained) SimpleTableViewController* Controller;
@property (nonatomic, readonly) NSArray* Cells;
@property (nonatomic, strong) NSString* Caption;
@property (nonatomic, strong) UIView* HeaderView;
@property (nonatomic, strong) UIView* FooterView;
@property (nonatomic, strong) NSString* Key;

- (CGFloat)calculateHeaderHeight:(CGFloat)width;
- (CGFloat)calculateFooterHeight:(CGFloat)width;
- (void)alignCells;
- (long)indexOfCell:(UITableViewCell*)cell;
- (void)addCell:(UITableViewCell*)cell;

- (UITableViewCell*)cellWithKey:(NSString*)Key;
- (void)setValue:(id)value forKey:(NSString*)key;
- (id)getValueForKey:(NSString*)key;

@end