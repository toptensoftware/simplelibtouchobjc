//
//  SimpleNoSelectCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleNoSelectCell.h"

@implementation SimpleNoSelectCell

- (BOOL)didSelect:(UIViewController*)viewController
{
	return NO;
}

- (BOOL)willSelect
{
	return NO;
}

- (BOOL)canBecomeFirstResponder
{
	return NO;
}

@end
