//
// Created by Brad Robinson on 30/05/13.


#import <Foundation/Foundation.h>
#import "SimpleEditCell.h"


@interface SimplePickerViewCell : SimpleEditCell<UIPickerViewDataSource, UIPickerViewDelegate>

- (id)initWithLabel:(NSString*)labelText;
- (id)initWithLabel:(NSString*)labelText andValues:(NSArray*)values andSelectedIndex:(int)index;

- (void)setValues:(NSArray*)values;
- (void)setSelectedIndex:(long)selectedIndex animated:(BOOL)animated;
- (void)setSelectedValue:(NSString*)value animated:(BOOL)animated;


@property (nonatomic, assign) long selectedIndex;

@property (nonatomic, copy) void (^ChangedBlock)(long);

@end