//
//  SimpleShareCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleShareCell.h"
#import "Logger.h"
#import "MobilizeAppDelegate.h"
#import "SHK.h"

@implementation SimpleShareCell

- (id)init
{
	if (!(self=[super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil]))
		return nil;
	self.textLabel.textColor=[[[UIColor new] initWithRed:0.25 green:.38 blue:.52 alpha:1] autorelease];
	self.textLabel.text = NSLocalizedString(@"Tell your friends", @"Tell your friends");
	return self;
}


- (BOOL)didSelect:(UIViewController*)viewController
{
    // Create the item to share (in this example, a url)
	NSURL *url = [NSURL URLWithString:@"http://www.tingofamily.com"];
	SHKItem *item = [SHKItem URL:url title:NSLocalizedString(@"I know where my kids are. #TingoFamily ", @"I know where my kids are. #TingoFamily ")];
    
	// Get the ShareKit action sheet
	SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
    
	// Display the action sheet
	[actionSheet showInView:self];
	return YES;
}

- (BOOL)shouldResignFirstResponder
{
	return TRUE;
}

- (void)dealloc {
    [super dealloc];
}


@end
