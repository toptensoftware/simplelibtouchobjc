//
//  SimpleNameValueCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleNameValueCell.h"
#import "SimpleAppearance.h"
#import "NSString+boundingSize.h"

@implementation SimpleNameValueCell


- (void)setupAppearance
{
    // Setup appearance
    SimpleAppearance* a = [SimpleAppearance Instance];
    if (a!=nil)
    {
        self.textLabel.font = a.labelFont;
        self.textLabel.textColor = a.labelColor;
        self.detailTextLabel.font = a.valueFont;
        self.detailTextLabel.textColor = a.valueColor;
    }

}


- (id)initWithName:(NSString*)name andValue:(NSString*)value
{
	if (!(self=[super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil]))
		return nil;
	
	self.textLabel.text=name;
	self.detailTextLabel.text=value;

    [self setupAppearance];

    return self;
}

- (id)initWithName:(NSString*)name andValue:(NSString*)value andViewClass:(Class)viewClass
{
	if (!(self=[self initWithName:name andValue:value]))
		return nil;
		
	m_viewClass=viewClass;
	
	return self;
}


- (BOOL)didSelect:(UIViewController*)viewController
{
	if (m_viewClass!=nil)
	{
		UIViewController* pNewController=[m_viewClass new];
		[viewController.navigationController pushViewController:pNewController animated:YES];
	}
	return YES;
}

- (BOOL)shouldResignFirstResponder
{
	return TRUE;
}


- (void)setValue:(id)value animate:(BOOL)animate
{
	self.detailTextLabel.text=(NSString*)value;
	
	if (animate)
	{	
		CGSize constraintSize = CGSizeMake(self.frame.size.width, 44);
		CGSize labelSize = [self.detailTextLabel.text boundingSizeWithFont:self.detailTextLabel.font constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
		
		CGRect startRect = CGRectMake(self.frame.size.width - labelSize.width - 50, self.frame.size.height / 2, 1, 1);
		
		self.detailTextLabel.frame = startRect;
		self.detailTextLabel.alpha = 0.0;
		
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:1.0];
	}
	
	[self setNeedsLayout];
	[self layoutIfNeeded];
	
	if (animate)
	{
		self.detailTextLabel.alpha = 1.0;
		[UIView commitAnimations];
	}
		
}

- (id)getValue
{
	return self.detailTextLabel.text;
}




@end
