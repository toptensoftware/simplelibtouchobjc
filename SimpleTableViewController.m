//
//  SimpleTableViewController.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleTableViewController.h"
#import "SimpleTableViewSection.h"
#import "SimpleAppearance.h"
#import "SimpleTableViewCell.h"
#import "SimpleKeyboardNavBar.h"

UIView* findFirstResponder(UIView* view)
{
    if (view.isFirstResponder)
        return view;
    
    for (UIView *subView in view.subviews) 
    {
        UIView* fr = findFirstResponder(subView);
        if (fr!=nil)
            return fr;
    }
    return nil;
}

void findAndResignFirstResponder(UIView* view)
{
    UIView* fr = findFirstResponder(view);
    if (fr!=nil)
        [fr resignFirstResponder];
}

UITableViewCell* findParentTableViewCell(UIView* view)
{
    if (view==nil)
        return nil;
    
    if ([view isKindOfClass:[UITableViewCell class]])
         return (UITableViewCell*)view;
    
    return findParentTableViewCell(view.superview);
}


@interface SimpleTableViewController() {
    BOOL _needsAlignment;
    BOOL _inResignFirstResponder;
    SimpleFormDoneBlock _doneBlock;
    SimpleFormDoneBlock _disappearBlock;
    SimpleKeyboardNavBar* m_keyboardNavBar;
}
@property (nonatomic, strong) NSMutableArray* _sections;

@end

@implementation SimpleTableViewController

@synthesize _sections;

- (id)init
{
	if (!(self=[super initWithStyle:UITableViewStyleGrouped]))
		return nil;

    self._sections = [[NSMutableArray alloc] init];

    m_keyboardNavBar = nil;

    // Apply background color
    SimpleAppearance* a = [SimpleAppearance Instance];
    if (a!=nil && a.backgroundColor!=nil)
    {
        self.tableView.backgroundColor = a.backgroundColor;
        self.tableView.backgroundView = nil;
    }

    _needsAlignment = YES;

	return self;
}

- (void)alignCells
{
    if (self.AlignAllSections)
    {
        float max = 0;
        for (SimpleTableViewSection* s in self._sections)
        {
            float thisAlign = s.calcPreferredAlignment;
            if (thisAlign > max)
                max = thisAlign;
        }
        for (SimpleTableViewSection* s in self._sections)
        {
            [s setActualAlignment:max];
        }
    }
    else
    {
        for (SimpleTableViewSection* s in self._sections)
        {
            [s alignCells];
        }
    }
        _needsAlignment = NO;
}

- (void)setNeedsCellAlignment
{
    _needsAlignment = YES;
}


- (void)deleteAllSections
{
	[self._sections removeAllObjects];
}

- (void)addSection:(SimpleTableViewSection*)section
{
    section.Controller = self;
    [self._sections addObject:section];
}

- (void)insertSection:(SimpleTableViewSection*)section atIndex:(int)index withRowAnimation:(UITableViewRowAnimation)animation
{
    section.Controller = self;
    [self setNeedsCellAlignment];
    [self._sections insertObject:section atIndex:index];
    [self.tableView insertSections:[[NSIndexSet alloc] initWithIndex:index] withRowAnimation:animation];
}

- (void)addDoneButton:(SimpleFormDoneBlock)block
{
    _doneBlock = block;

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onDone)];
}

- (void)applyOnDisappear:(SimpleFormDoneBlock)block
{
    _disappearBlock = block;
}


- (void)onDone
{
    if (_doneBlock(self))
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (UITableViewCell*)cellWithKey:(NSString*)key
{
    for (SimpleTableViewSection* s in self._sections)
    {
        for (UITableViewCell* cell in s.Cells)
        {
            if ([cell isKindOfClass:[SimpleTableViewCell class]])
            {
                if ([key isEqual:((SimpleTableViewCell*)cell).Key])
                    return cell;
            }
        }
    }

    return nil;
}

- (void)setValue:(id)value forKey:(NSString*)key
{
    UITableViewCell* cell = [self cellWithKey:key];
    if (cell!=nil && [cell conformsToProtocol:@protocol(HasValue)])
    {
        return [((id <HasValue>) cell) setValue:value animate:YES];
    }
}

- (id)getValueForKey:(NSString*)key
{
    UITableViewCell* cell = [self cellWithKey:key];
    if (cell!=nil && [cell conformsToProtocol:@protocol(HasValue)])
    {
        return [((id <HasValue>) cell) getValue];
    }
    return nil;
}



#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return [self._sections count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return ((SimpleTableViewSection*)[self._sections objectAtIndex:section]).Caption;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	return ((SimpleTableViewSection*)[self._sections objectAtIndex:section]).HeaderView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return ((SimpleTableViewSection*)[self._sections objectAtIndex:section]).FooterView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	SimpleTableViewSection* s=[self._sections objectAtIndex:section];
    CGFloat height = [s calculateHeaderHeight:tableView.frame.size.width];
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	SimpleTableViewSection* s=[self._sections objectAtIndex:section];
	CGFloat height = [s calculateFooterHeight:tableView.frame.size.width];
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell* pCell=[self tableView:tableView cellForRowAtIndexPath:indexPath];
	
	CGFloat ret;
	if ([pCell conformsToProtocol:@protocol(MeasureHeight)])
	{
		ret = [(id<MeasureHeight>)pCell measureHeightForWidth:tableView.frame.size.width];
	}
	else
	{
		if (pCell)
			ret = pCell.frame.size.height;
		else
			ret = 90;
	}
	
	//NSLog (@"SimpleTableViewController : height for %d,%d is %f", indexPath.section, indexPath.row, ret);
	return ret;
}



// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    return ((SimpleTableViewSection*)[self._sections objectAtIndex:section]).Cells.count;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    // Realign celles?
    if (_needsAlignment)
        [self alignCells];

    return [((SimpleTableViewSection*)[self._sections objectAtIndex:indexPath.section]).Cells objectAtIndex:indexPath.row];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell* cell=[self tableView:tableView cellForRowAtIndexPath:indexPath];
	
	// Check if other first responders should be resigned when this cell is selected
	if ([cell respondsToSelector:@selector(shouldResignFirstResponder)])
	{
		if ([(id<SelectHandler>)cell shouldResignFirstResponder])
		{
			[self resignFirstResponder];
		}
	}

	// Check if this cell wants to take selection
	if ([cell respondsToSelector:@selector(willSelect)])
	{
		if (![(id<SelectHandler>)cell willSelect])
			return nil;	// Select nothing
	}
	
	// Allow cell to be selected
	return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	UITableViewCell* cell=[self tableView:tableView cellForRowAtIndexPath:indexPath];
	if ([cell conformsToProtocol:@protocol(SelectHandler)])
	{
		if (![(id<SelectHandler>)cell didSelect:self])
			return;
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)deleteSectionAtIndex:(int)index withRowAnimation:(UITableViewRowAnimation)animation
{
    // Remove the section
    [self._sections removeObjectAtIndex:index];

    // Update table
    [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:index] withRowAnimation:animation];
}


- (SimpleTableViewSection*)findSection:(NSString*)key
{
	for (SimpleTableViewSection* s in self._sections)
	{
		if ([s.Key isEqual:key])
			return s;
	}
	return nil;
}

- (long)findCheckedCellInTable
{
	for (SimpleTableViewSection* s in self._sections)
	{
		for (UITableViewCell* cell in s.Cells)
		{
			if (cell.accessoryType==UITableViewCellAccessoryCheckmark)
				return cell.tag;
		}
	}
	return -1;
}

- (long)findCheckedCellInSection:(NSString*)key
{
	SimpleTableViewSection* s=[self findSection:key];
	for (UITableViewCell* cell in s.Cells)
	{
		if (cell.accessoryType==UITableViewCellAccessoryCheckmark)
			return cell.tag;
	}
	return -1;
}

- (void)resignFirstResponder
{
    _inResignFirstResponder = YES;
	findAndResignFirstResponder(self.view);
    [self.view endEditing:YES];
    _inResignFirstResponder = NO;
}

- (UITableViewCell*)findNextResponder:(UIView*)fromView
{
    UITableViewCell* from = findParentTableViewCell(fromView);

	long row=0;
	long section=0;
	if (from!=nil)
	{
		NSIndexPath* indexPath;
		indexPath=[self indexPathOfCell:from];
		if (!indexPath)
			return nil;
		
		row=indexPath.row+1;
		section=indexPath.section;
	}
    
	for (; section<self._sections.count; section++)
	{
		NSArray* cells=((SimpleTableViewSection*)[self._sections objectAtIndex:section]).Cells;
		for (;row<cells.count; row++)
		{
			UITableViewCell* cell=[cells objectAtIndex:row];
			if ([cell conformsToProtocol:@protocol(FirstResponder)])
			{
                return cell;
			}
		}
		
		row=0;
	}

	return nil;
}

- (UITableViewCell*)findPrevResponder:(UIView*)fromView
{
    UITableViewCell* from = findParentTableViewCell(fromView);

	long section=self._sections.count-1;
	long row=((SimpleTableViewSection*)[self._sections objectAtIndex:section]).Cells.count-1;
	if (from!=nil)
	{
		NSIndexPath* indexPath;
		indexPath=[self indexPathOfCell:from];
		if (!indexPath)
			return nil;
		
		row=indexPath.row-1;
		section=indexPath.section;
	}
    
	for (; section>=0; section--)
	{
		NSArray* cells=((SimpleTableViewSection*)[self._sections objectAtIndex:section]).Cells;
		for (;row>=0; row--)
		{
			UITableViewCell* cell=[cells objectAtIndex:row];
			if ([cell conformsToProtocol:@protocol(FirstResponder)])
			{
                return cell;
			}
		}
		
        if (section>0)
        {
            SimpleTableViewSection* prevSection = (SimpleTableViewSection*)[self._sections objectAtIndex:section-1];
            row = prevSection.Cells.count-1;
        }
	}

    return nil;
}

- (NSIndexPath*)indexPathOfCell:(UITableViewCell*)cell
{
    for (int i=0; i<self._sections.count; i++)
    {
        long index = [((SimpleTableViewSection *) [self._sections objectAtIndex:i]) indexOfCell:cell];
        if (index!=NSNotFound)
        {
            return [NSIndexPath indexPathForRow:index inSection:i];
        }
    }
    return nil;
}

- (SimpleTableViewSection*)sectionOfCell:(UITableViewCell*)cell
{
    for (SimpleTableViewSection* section in self._sections)
    {
        if ([section indexOfCell:cell]!=NSNotFound)
            return section;
    }
    return nil;
}

- (unsigned long)indexOfSection:(SimpleTableViewSection*)section
{
    return [self._sections indexOfObject:section];
}

- (SimpleTableViewSection*)sectionAtIndex:(int)index
{
    return [self._sections objectAtIndex:index];
}





- (BOOL)setFirstResponder:(UITableViewCell*)cell
{
    if (cell!=nil && [cell conformsToProtocol:@protocol(FirstResponder)])
    {
        id<FirstResponder> fr=(id<FirstResponder>)cell;
        if ([fr canBecomeFirstResponder])
        {
            [self.tableView scrollToRowAtIndexPath:[self indexPathOfCell:cell] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            [fr takeFirstResponder];
            return YES;
        }
    }
    
    [self resignFirstResponder];
    return NO;
}

- (BOOL)moveToNextFirstResponder:(UIView*)from
{
    return [self setFirstResponder:[self findNextResponder:from]];
}

- (BOOL)moveToPrevFirstResponder:(UIView*)from
{
    return [self setFirstResponder:[self findPrevResponder:from]];
}

- (UITableViewCell*)findFirstResponder
{
    return findParentTableViewCell(findFirstResponder(self.view));
}

- (NSArray*)allCells
{
	NSMutableArray* array=[NSMutableArray new];
	for (SimpleTableViewSection* s in self._sections)
	{
		[array addObjectsFromArray:s.Cells];
	}
	
	return array;
}

- (SimpleKeyboardNavBar*)keyboardNavBar
{
    if (m_keyboardNavBar==nil)
    {
        m_keyboardNavBar = [[SimpleKeyboardNavBar alloc] initWithController:self];
    }
    return m_keyboardNavBar;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    if (_disappearBlock!=nil)
    {
        _disappearBlock(self);
    }
}


@end

