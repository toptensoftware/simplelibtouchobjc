//
//  SimpleActionCell.h
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleTableViewCell.h"
#import "SimpleTableViewController.h"

@interface SimpleActionCell : SimpleTableViewCell<SelectHandler, MeasureHeight>
{
    id _selectorTarget;
    SEL _selector;
}

- (id)initWithText:(NSString*)text andTarget:(id)target andSelector:(SEL)selector;

- (CGFloat)measureHeightForWidth:(CGFloat)width;


@property (nonatomic) BOOL MultiLine;

@end
