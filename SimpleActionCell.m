//
//  SimpleActionCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleActionCell.h"
#import "SimpleAppearance.h"
#import "NSString+boundingSize.h"

@implementation SimpleActionCell

@synthesize MultiLine;

- (void)setupAppearance
{
    // Setup appearance
    SimpleAppearance* a = [SimpleAppearance Instance];
    if (a!=nil)
    {
        self.textLabel.font = a.labelFont;
        self.textLabel.textColor = a.labelColor;
    }

}
- (id)initWithText:(NSString*)text andTarget:(id)target andSelector:(SEL)selector;
{
	if (!(self=[super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil]))
		return nil;
	
	self.textLabel.textColor=[[UIColor new] initWithRed:0.25 green:.38 blue:.52 alpha:1];
	self.textLabel.text=text;
    self.textLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    _selectorTarget = target;
    _selector = selector;

    [self setupAppearance];

	return self;
}

- (BOOL)didSelect:(UIViewController*)viewController
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [_selectorTarget performSelector:_selector];
#pragma clang diagnostic pop
	return YES;
}

- (BOOL)shouldResignFirstResponder
{
	return TRUE;
}

- (void)layoutSubviews
{
    if (MultiLine)
    {
        self.textLabel.numberOfLines = 0;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        CGSize size = [self.textLabel.text boundingSizeWithFont:self.textLabel.font constrainedToSize:CGSizeMake(self.bounds.size.width-30, 10000) lineBreakMode:NSLineBreakByWordWrapping];
        self.textLabel.frame = CGRectMake(20, 10, size.width, size.height);
    }
    [super layoutSubviews];
}

- (CGFloat)measureHeightForWidth:(CGFloat)width
{
    if (MultiLine)
    {
        CGSize size = [self.textLabel.text boundingSizeWithFont:self.textLabel.font constrainedToSize:CGSizeMake(width-30, 10000) lineBreakMode:NSLineBreakByWordWrapping];
        return size.height + 20;
    }
    else
        return 44;
}

- (UIAccessibilityTraits)accessibilityTraits
{
    return UIAccessibilityTraitButton;
}


@end
