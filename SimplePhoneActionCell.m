//
//  SimplePhoneAction.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimplePhoneActionCell.h"


@implementation SimplePhoneActionCell


- (id)initWithPhoneNumber:(NSString*)number
{
	if (!(self=[super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil]))
		return nil;
	
	m_phoneNumber=number;
	
	self.textLabel.textColor=[[UIColor new] initWithRed:0.25 green:.38 blue:.52 alpha:1];
	self.textLabel.text=[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Call", @"Call on the phone"), number];
	
	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"handset.png"]];
	self.accessoryView = imageView;

	return self;
}


- (BOOL)didSelect:(UIViewController*)viewController
{
	NSString* phoneNumber=[m_phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
	phoneNumber=[phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
	phoneNumber=[phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneNumber]]];
	return YES;
}

- (BOOL)shouldResignFirstResponder
{
	return TRUE;
}



@end
