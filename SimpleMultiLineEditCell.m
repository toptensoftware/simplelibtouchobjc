//
//  SimpleEditCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleMultiLineEditCell.h"
#import "SimpleAppearance.h"

@interface SimpleMultiLineEditCell()
{
    UITextView*	m_editor;
}
@end

@implementation SimpleMultiLineEditCell

- (void)setupAppearance
{
    // Setup appearance
    SimpleAppearance* a = [SimpleAppearance Instance];
    if (a!=nil)
    {
        m_editor.font = a.valueFont;
        m_editor.textColor = a.valueColor;
    }

}

- (id)initWithText:(NSString*)text
{
    if (!(self=[super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil]))
        return nil;

    // Create the editor
    m_editor=[[UITextView alloc] initWithFrame:CGRectMake(0, 0, 200, 200) ];
    m_editor.text=text;

    [self.contentView addSubview:m_editor];

    self.selectionStyle=UITableViewCellSelectionStyleNone;

    [self setupAppearance];
    m_editor.backgroundColor = [UIColor clearColor];

    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    m_editor.frame = self.contentView.bounds;
}

- (void)setValue:(id)value animate:(BOOL)animate
{
	if ([value isKindOfClass:[NSString class]])
		m_editor.text=(NSString*)value;
}

- (id)getValue
{
	return m_editor.text;
}

- (BOOL)didSelect:(UIViewController*)viewController
{
	return YES;
}

- (BOOL)willSelect
{
	[m_editor becomeFirstResponder];
	return FALSE;
}

- (void)takeFirstResponder
{
	[m_editor becomeFirstResponder];
}

- (BOOL)canBecomeFirstResponder
{
	return YES;
}

- (CGFloat)measureHeightForWidth:(CGFloat)width
{
    return 180;
}



- (UITextView*)editor
{
	return m_editor;
}

- (NSString*)TextValue
{
    return m_editor.text;
}

- (void)setTextValue:(NSString *)value
{
    m_editor.text = value;
}

@end
