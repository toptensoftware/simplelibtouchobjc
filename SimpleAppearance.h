//
// Created by Brad Robinson on 30/05/13.


#import <UIKit/UIKit.h>


@interface SimpleAppearance : NSObject

@property (nonatomic, strong) UIFont* valueFont;
@property (nonatomic, strong) UIColor* valueColor;
@property (nonatomic, strong) UIFont* labelFont;
@property (nonatomic, strong) UIColor* labelColor;
@property (nonatomic, strong) UIFont* hintFont;
@property (nonatomic, strong) UIColor* hintColor;
@property (nonatomic, strong) UIColor* backgroundColor;

+ (SimpleAppearance*)Instance;
+ (void)setInstance:(SimpleAppearance*)value;

@end