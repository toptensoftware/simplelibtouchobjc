//
//  SimpleTableViewCell.h
//
//  Created by Brad Robinson on 3/05/10.
//  Copyright 2010 Cognethos Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SimpleTableViewController;


@interface SimpleTableViewCell : UITableViewCell 
{
	UILabel * m_badgeLabel;
}

@property (nonatomic, strong) NSString* Key;
@property (nonatomic, unsafe_unretained) id Tag;
@property (nonatomic, unsafe_unretained) SimpleTableViewController* Controller;

- (void)setBadge:(int)value;
- (void)scrollVisible:(BOOL)animated;

@end
