//
//  SimpleDateTimeCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleDateTimeCell.h"
#import "Util.h"
#import "NSString+boundingSize.h"

@implementation SimpleDateTimeCell

- (id)initWithLabel:(NSString *)label andDate:(NSDate*)date andType:(DateTimeType)type andParentView:(UITableView*)view
{
	if (!(self=[super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil]))						
		return nil;
	
	// Swap colors of the text and detail text labels
	UIColor* temp=self.textLabel.textColor;
	self.textLabel.textColor=self.detailTextLabel.textColor;
	self.detailTextLabel.textColor=temp;
	self.textLabel.text=label;
	
	// Make the text label smaller
	self.textLabel.font=[UIFont systemFontOfSize:13];
	self.detailTextLabel.text = @"";

	m_datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 156, 320.0, 216.0)];

	switch ((int)type)
	{
		case timeType:
			m_datePicker.datePickerMode = UIDatePickerModeTime;
			m_format = @"hh:mm a";
			break;
		case dateType:
			m_datePicker.datePickerMode = UIDatePickerModeDate;
			m_format = @"ccc d MMM";
			break;
		case dateTimeType:
		default:
			m_datePicker.datePickerMode = UIDatePickerModeDateAndTime;
			m_format = @"ccc d MMM YYYY hh:mm a";
			break;
	}

	// we need to disable scrolling on the parent view because scrolling messes up the date picker 
	m_parentTableView = view;
	[m_parentTableView setScrollEnabled:NO];
	
	NSLog (@"SimpleDateTimeCell %@", date);
	m_datePicker.date = (date ? date : [NSDate date]);
    [m_datePicker addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    [m_parentTableView addSubview:m_datePicker];
	[self updateDate];
	
	return self;
}

- (void) updateDate
{
	NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:m_format];
	if ([m_datePicker date])
		self.detailTextLabel.text = [[NSString alloc] initWithString:[dateFormatter stringFromDate:[m_datePicker date]]];
}

-(IBAction)dateChanged
{
	[self updateDate];
	
	BOOL selected = self.selected;
	NSLog (@"Selected %@", (selected ? @"true" : @"false"));
	
	// reload the table
//	[m_parentTableView reloadData];
	
	NSLog (@"Selected %@", (self.selected ? @"true" : @"false"));
}


- (void)setValue:(id)value animate:(BOOL)animate
{
	if ([value isKindOfClass:[NSDate class]])
	{
		m_datePicker.date =(NSDate*)value;
		//[m_parentTableView reloadData];
	}
}

- (id)getValue
{
	return m_datePicker.date;
}

- (BOOL)didSelect:(UIViewController*)viewController
{
	// show our datepicker
	[m_parentTableView bringSubviewToFront:m_datePicker];
	return NO;
}

- (BOOL)willSelect
{
	return YES;
}

- (void)takeFirstResponder
{
	NSIndexPath * indexPath=[m_parentTableView indexPathForCell:self];
	[m_parentTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
	[m_parentTableView bringSubviewToFront:m_datePicker];
}

- (BOOL)canBecomeFirstResponder
{
	return YES;
}

- (CGFloat)measureHeightForWidth:(CGFloat)width
{
	CGFloat fontHeight=[@"X" boundingSizeWithFont:self.textLabel.font].height+10;
	if (fontHeight<44)
		return 44;
	else
		return fontHeight;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	SimpleTableViewController* viewController=(SimpleTableViewController*)[self.superview nextResponder];
    if (![viewController isKindOfClass:[SimpleTableViewController class]])
		return NO;	
	
	[viewController moveToNextFirstResponder:self];
	return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
	return YES;
}

@end
