//
//  SimpleEmailActionCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleEmailActionCell.h"


@implementation SimpleEmailActionCell


- (id)initWithEmail:(NSString*)email
{
	if (!(self=[super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil]))
		return nil;
	
	m_email=email;
	
	self.textLabel.textColor=[[UIColor new] initWithRed:0.25 green:.38 blue:.52 alpha:1];
	self.textLabel.text=email;
	
	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"email.png"]];
	self.accessoryView = imageView;

	return self;
}


- (BOOL)didSelect:(UIViewController*)viewController
{
	NSString* email=[NSString stringWithFormat:@"mailto:?to=%@", m_email];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
	return YES;
}

- (BOOL)shouldResignFirstResponder
{
	return TRUE;
}



@end
