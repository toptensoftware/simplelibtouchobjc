//
//  SimpleDisclosureCell.h
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleTableViewCell.h"
#import "SimpleTableViewController.h"

typedef void (^ActionBlock)();

@interface SimpleDisclosureCell : SimpleTableViewCell<SelectHandler, MeasureHeight, HasValue, HasAlignment>

- (id)initWithText:(NSString*)text;

- (CGFloat)measureHeightForWidth:(CGFloat)width;

@property (nonatomic, strong) Class ViewControllerClass;
@property (nonatomic, copy) ActionBlock SelectedBlock;
@property (nonatomic, assign) BOOL ReuseViewController;
@property (nonatomic, assign) BOOL MultiLine;
@property (nonatomic, assign) NSString* Label;
@property (nonatomic, strong) NSString* Value;
@property (nonatomic, strong) NSString* SubText;
@property (nonatomic, assign) BOOL AlignValue;

@end
