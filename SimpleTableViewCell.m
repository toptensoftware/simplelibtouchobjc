//
//  SimpleTableViewCell.m
//
//  Created by Brad Robinson on 3/05/10.
//  Copyright 2010 Cognethos Pty Ltd. All rights reserved.
//

#import "SimpleTableViewCell.h"
#import "SimpletableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+boundingSize.h"

@implementation SimpleTableViewCell

- (void) addBadge
{
	if (!m_badgeLabel)
	{
		// create our badge
		m_badgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(243, 13, 32, 20)];
		
		// corners
		m_badgeLabel.layer.masksToBounds = YES;
		m_badgeLabel.layer.cornerRadius = 9.0;
		
		m_badgeLabel.backgroundColor=[[UIColor new] initWithRed:0.25 green:.38 blue:.52 alpha:1];
		m_badgeLabel.textColor=[UIColor whiteColor];
		m_badgeLabel.hidden = YES;
		m_badgeLabel.textAlignment = NSTextAlignmentCenter;
		[self addSubview:m_badgeLabel];
	}
}

- (void)setBadge:(int)value
{	
	[self addBadge];
	
	if (!value)
	{
		m_badgeLabel.hidden = YES;
		return;
	}
	
	m_badgeLabel.text=[NSString stringWithFormat:@"%d", value];
	m_badgeLabel.hidden = NO;
	
	// reset the rectangle
	CGFloat textWidth=[m_badgeLabel.text boundingSizeWithFont:m_badgeLabel.font].width+12;
	m_badgeLabel.frame=CGRectMake(m_badgeLabel.frame.origin.x + m_badgeLabel.frame.size.width - textWidth,
								m_badgeLabel.frame.origin.y,
								textWidth,
								m_badgeLabel.frame.size.height);

}




- (void)scrollVisible:(BOOL)_animated
{
    // Get the table view controller
	SimpleTableViewController* viewController=(SimpleTableViewController*)[self.superview nextResponder];
	if (![viewController isKindOfClass:[SimpleTableViewController class]])
		return;

    // Find this cell's indexpath and row
    [viewController.tableView scrollToRowAtIndexPath:[viewController.tableView indexPathForCell:self] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}


@end
