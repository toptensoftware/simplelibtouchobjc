//
//  SimpleNameValueCell.h
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleTableViewCell.h"
#import "SimpleTableViewController.h"

@interface SimpleNameValueCell : SimpleTableViewCell<SelectHandler, HasValue>
{
	Class	m_viewClass;
}

- (id)initWithName:(NSString*)name andValue:(NSString*)value;
- (id)initWithName:(NSString*)name andValue:(NSString*)value andViewClass:(Class)viewClass;

@end
