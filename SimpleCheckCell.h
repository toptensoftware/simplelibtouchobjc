//
//  SimpleCheckCell.h
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimpleTableViewCell.h"
#import "SimpleTableViewController.h"

typedef enum
{
	SimpleExclusiveModeNone,
	SimpleExclusiveModeGroup,
	SimpleExclusiveModeTable,
} SimpleExclusiveMode;

typedef void (^SimpleCellSelectedBlock)(UITableViewCell*);

@interface SimpleCheckCell : SimpleTableViewCell<SelectHandler, HasValue> {

	BOOL m_enabled;
	SimpleExclusiveMode m_exclusiveMode;
}

- (id)initWithName:(NSString*)name checked:(BOOL)checked;
- (SimpleExclusiveMode)exclusiveMode;
- (void)setExclusiveMode:(SimpleExclusiveMode)value;
- (BOOL)checked;
- (void)setChecked:(BOOL)value;
- (BOOL)enabled;
- (void)setEnabled:(BOOL)value;

@property (nonatomic, copy) SimpleCellSelectedBlock SelectedBlock;




@end
