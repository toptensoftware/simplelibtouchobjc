//
//  SimpleTableViewController.h
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef BOOL (^SimpleFormDoneBlock)(UIViewController* controller);

@class SimpleTableViewSection;

// These protocols can be implemented to table view cells to control behaviour

// If implemented allows calculation of the cells height for a particular table with
@protocol MeasureHeight
- (CGFloat)measureHeightForWidth:(CGFloat)width;
@end

// Get/set the value of the cell
@protocol HasValue
- (void)setValue:(id)value animate:(BOOL)animate;
- (id)getValue;
@end

// Selection handling
@protocol SelectHandler
// Called when selected, typically pushes a new view
- (BOOL)didSelect:(UIViewController*)viewController;		
@optional
// Called before selection, return FALSE to cancel row selection
- (BOOL)willSelect;											
// Return true to cause other responders to be resigned when this row is selected
- (BOOL)shouldResignFirstResponder;
@end

@protocol FirstResponder
- (void)takeFirstResponder;
- (BOOL)canBecomeFirstResponder;
@end

@protocol HasAlignment
- (CGFloat)calcPreferredAlignment;
- (void)setActualAlignment:(CGFloat)alignment;
@end

@class SimpleTableViewController;
@class SimpleKeyboardNavBar;


@interface SimpleTableViewController : UITableViewController
{
}

// Reset
- (void)deleteAllSections;
- (void)addSection:(SimpleTableViewSection*)section;
- (void)insertSection:(SimpleTableViewSection*)section atIndex:(int)index withRowAnimation:(UITableViewRowAnimation)animation;
- (void)deleteSectionAtIndex:(int)index withRowAnimation:(UITableViewRowAnimation)animation;
- (SimpleTableViewSection*)findSection:(NSString*)key;
- (void)alignCells;
- (void)setNeedsCellAlignment;
- (void)addDoneButton:(SimpleFormDoneBlock)block;
- (void)applyOnDisappear:(SimpleFormDoneBlock)block;
- (NSIndexPath*)indexPathOfCell:(UITableViewCell*)cell;
- (SimpleTableViewController*)sectionOfCell:(UITableViewCell*)cell;
- (unsigned long)indexOfSection:(SimpleTableViewSection*)section;
- (SimpleTableViewSection*)sectionAtIndex:(int)index;

- (UITableViewCell*)cellWithKey:(NSString*)key;

- (void)setValue:(id)value forKey:(NSString*)key;
- (id)getValueForKey:(NSString*)key;

- (SimpleKeyboardNavBar*)keyboardNavBar;

// Operations
- (void)resignFirstResponder;
- (UITableViewCell*)findNextResponder:(UIView*)from;
- (UITableViewCell*)findPrevResponder:(UIView*)from;
- (BOOL)moveToNextFirstResponder:(UIView*)from;
- (BOOL)moveToPrevFirstResponder:(UIView*)from;
- (UITableViewCell*)findFirstResponder;
- (BOOL)setFirstResponder:(UITableViewCell*)cell;

@property (nonatomic, assign) BOOL AlignAllSections;

@end
