//
//  SimpleWebActionCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleWebActionCell.h"


@implementation SimpleWebActionCell


- (id)initWithWeb:(NSString*)Web
{
	return [self initWithWeb:Web andLabel:Web];
}

- (id)initWithWeb:(NSString*)Web andLabel:(NSString*)Label
{
	if (!(self=[super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil]))
		return nil;
	
	m_web=Web;
	
	self.textLabel.textColor=[[UIColor new] initWithRed:0.25 green:.38 blue:.52 alpha:1];
	self.textLabel.text=[NSString stringWithFormat:@"%@", Label];
	
	//UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Web.png"]];
	//self.accessoryView = imageView;
	//[imageView release];
	
	return self;
}


- (BOOL)didSelect:(UIViewController*)viewController
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:m_web]];
	return YES;
}

- (BOOL)shouldResignFirstResponder
{
	return TRUE;
}



@end
