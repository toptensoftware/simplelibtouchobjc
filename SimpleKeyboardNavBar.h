//
// Created by Brad Robinson on 30/05/13.


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class SimpleTableViewController;


@interface SimpleKeyboardNavBar : UIToolbar

- (id)initWithController:(SimpleTableViewController*)controller;
- (void)enableNext:(BOOL)value;
- (void)enablePrev:(BOOL)value;

@end