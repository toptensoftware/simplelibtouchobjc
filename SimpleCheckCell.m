//
//  SimpleCheckCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleCheckCell.h"
#import "SimpleAppearance.h"


@implementation SimpleCheckCell

- (id)initWithName:(NSString*)name checked:(BOOL)value
{
	if (!(self=[super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil]))
		return nil;
	
	self.textLabel.text=name;
	self.checked=value;
	
	m_enabled=YES;
	m_exclusiveMode=SimpleExclusiveModeNone;

    SimpleAppearance * a = [SimpleAppearance Instance];
    if (a!=nil)
    {
        self.textLabel.font = a.valueFont;
        self.textLabel.textColor = a.valueColor;
    }

    return self;
}

- (SimpleExclusiveMode)exclusiveMode
{
	return m_exclusiveMode;
}

- (void)setExclusiveMode:(SimpleExclusiveMode)value
{
	m_exclusiveMode=value;
}

- (BOOL)didSelect:(UIViewController*)viewController
{
	if (!m_enabled)
		return YES;
	
	if (m_exclusiveMode==SimpleExclusiveModeNone)
	{
		// Non-exclusive, just toggle self
		if (self.accessoryType==UITableViewCellAccessoryCheckmark)
		{
			self.accessoryType=UITableViewCellAccessoryNone;
		}
		else
		{
			self.accessoryType=UITableViewCellAccessoryCheckmark;
		}
	}
	else if (m_exclusiveMode==SimpleExclusiveModeGroup)
	{
		// Group Exclusive, enumerate all sibling cells and turn off others
		UITableView* tableView=(UITableView*)[self superview];
		NSIndexPath *indexPath = [tableView indexPathForCell: self];
		long rows = [tableView numberOfRowsInSection:indexPath.section];
		for (long i=0; i<rows; i++)
		{
			UITableViewCell* cell=[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
			if ([cell isKindOfClass:[self class]])
			{
				if (i!=indexPath.row)
				{
					cell.accessoryType=UITableViewCellAccessoryNone;
				}
				else
				{
					cell.accessoryType=UITableViewCellAccessoryCheckmark;
				}
			}
		}
	}
	else 
	{
		// Table Exclusive, enumerate all cells and turn off others
		
		UITableView* tableView=(UITableView*)[self superview];
		NSIndexPath *indexPath = [tableView indexPathForCell: self];
		
		// Exclusive, enumerate all sibling cells and turn off others
		long sections=[tableView numberOfSections];
		for (long section=0; section<sections; section++)
		{
			long rows = [tableView numberOfRowsInSection:section];
			for (long i=0; i<rows; i++)
			{
				UITableViewCell* cell=[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:section]];
				if ([cell isKindOfClass:[self class]])
				{
					if (i==indexPath.row && section==indexPath.section)
					{
						cell.accessoryType=UITableViewCellAccessoryCheckmark;
					}
					else
					{
						cell.accessoryType=UITableViewCellAccessoryNone;
					}
				}
			}
		}
	}

    if (self.SelectedBlock)
    {
        self.SelectedBlock(self);
    }
	
	return YES;
}

- (BOOL)checked
{
	return (self.accessoryType==UITableViewCellAccessoryCheckmark);
}

- (void)setChecked:(BOOL)value
{
	self.accessoryType=value ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
}

- (BOOL)enabled
{
	return m_enabled;
}

- (void)setEnabled:(BOOL)value
{
	m_enabled=value;

	if (m_enabled)
	{
		self.textLabel.textColor=[[UIColor new] initWithRed:0.25 green:.38 blue:.52 alpha:1];
		self.selectionStyle=UITableViewCellSelectionStyleBlue;
	}
	else 
	{
		self.textLabel.textColor=[[UIColor new] initWithRed:0.75 green:.75 blue:.75 alpha:1];
		self.selectionStyle=UITableViewCellSelectionStyleNone;
	}

}

- (void)setValue:(id)value animate:(BOOL)animate
{
	if ([value isKindOfClass:[NSNumber class]])
	{
		self.checked=[(NSNumber*)value boolValue];
	}
}

- (id)getValue
{
	return [NSNumber numberWithBool:self.checked];
}


@end
