//
// Created by Brad Robinson on 30/05/13.


#import "SimpleAppearance.h"

static SimpleAppearance* _instance = nil;

@implementation SimpleAppearance {

}

+ (SimpleAppearance*)Instance
{
    return _instance;
}

+ (void)setInstance:(SimpleAppearance*)value
{
    _instance = value;
}

@end