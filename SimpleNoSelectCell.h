//
//  SimpleNoSelectCell.h
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimpleTableViewController.h"
#import "SimpleTableViewCell.h"

@interface SimpleNoSelectCell : SimpleTableViewCell<SelectHandler> {
}

@end
