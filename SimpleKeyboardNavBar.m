//
// Created by Brad Robinson on 30/05/13.


#import "SimpleKeyboardNavBar.h"
#import "SimpleTableViewController.h"


@implementation SimpleKeyboardNavBar {
    __unsafe_unretained SimpleTableViewController* _controller;

    UIBarButtonItem* _prevButton;
    UIBarButtonItem* _nextButton;
}

- (id)initWithController:(SimpleTableViewController*)controller
{
    self = [super initWithFrame:CGRectMake(0,0,320,44)];
    if (self==nil)
        return nil;

    // Store the controller
    _controller = controller;

    // Setup the bar style
    self.barStyle = UIBarStyleDefault;

    // Create the toolbar items
    _prevButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"PrevArrow.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(onPrevField)];
    _nextButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"NextArrow.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onNextField)];
    UIBarButtonItem* spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* hideButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"HideKeyboard.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onHideKeyboard)];

    _nextButton.tintColor = [UIColor blackColor];
    _prevButton.tintColor = [UIColor blackColor];
    hideButton.tintColor = [UIColor blackColor];

    // Setup the toolbar
    NSArray* items = [NSArray arrayWithObjects:_prevButton, _nextButton, spaceItem, hideButton, nil];
    [self setItems:items animated:NO];

    return self;
}

- (void)onPrevField
{
    [_controller moveToPrevFirstResponder:[_controller findFirstResponder]];
}

- (void)onNextField
{
    [_controller moveToNextFirstResponder:[_controller findFirstResponder]];
}

- (void)onHideKeyboard
{
    [_controller resignFirstResponder];
}

- (void)enableNext:(BOOL)value
{
    _nextButton.enabled = value;
}

- (void)enablePrev:(BOOL)value
{
    _prevButton.enabled = value;
}


@end