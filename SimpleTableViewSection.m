//
// Created by Brad Robinson on 30/05/13.


#import <UIKit/UIKit.h>
#import "SimpleTableViewSection.h"
#import "SimpleTableViewController.h"
#import "SimpleTableViewCell.h"


@implementation SimpleTableViewSection {

    NSMutableArray* _cells;
    __unsafe_unretained SimpleTableViewController* _controller;

}

- (id)init
{
    self = [super init];
    if (self==nil)
        return nil;

    _cells = [[NSMutableArray alloc] init];

    return self;
}

- (SimpleTableViewController*)Controller
{
    return _controller;
}

- (void)setController:(SimpleTableViewController*)Controller
{
    _controller = Controller;

    // Also pass to all cells
    for (UITableViewCell* cell in _cells)
    {
        if ([cell isKindOfClass:[SimpleTableViewCell class]])
        {
            ((SimpleTableViewCell*)cell).Controller = _controller;
        }
    }
}


- (CGFloat)calculateHeaderHeight:(CGFloat)width
{
    if (self.HeaderView!=nil)
    {
        if ([self.HeaderView conformsToProtocol:@protocol(MeasureHeight)])
        {
            return [((id <MeasureHeight>) self.HeaderView) measureHeightForWidth:width];
        }
        return self.HeaderView.frame.size.height;
    }

    return 0;
}

- (CGFloat)calculateFooterHeight:(CGFloat)width
{
    if (self.FooterView!=nil)
    {
        if ([self.FooterView conformsToProtocol:@protocol(MeasureHeight)])
        {
            return [((id <MeasureHeight>) self.FooterView) measureHeightForWidth:width];
        }
        return self.FooterView.frame.size.height;
    }

    return 0;
}

- (void)alignCells
{
    [self setActualAlignment:self.calcPreferredAlignment];

}

- (long)indexOfCell:(UITableViewCell*)cell
{
    return [_cells indexOfObject:cell];
}

- (void)addCell:(UITableViewCell*)cell
{
    [_cells addObject:cell];

    // Pass down controller reference
    if ([cell isKindOfClass:[SimpleTableViewCell class]])
    {
        ((SimpleTableViewCell*)cell).Controller = _controller;
    }
}

- (NSArray*)Cells
{
    return _cells;
}

- (UITableViewCell*)cellWithKey:(NSString*)Key
{
    for (UITableViewCell* cell in _cells)
    {
        if ([cell isKindOfClass:[SimpleTableViewCell class]])
        {
            if ([((SimpleTableViewCell*) cell).Key isEqualToString:Key])
                return cell;
        }
    }

    return nil;
}

- (void)setValue:(id)value forKey:(NSString*)key
{
    UITableViewCell* cell = [self cellWithKey:key];
    if (cell!=nil && [cell conformsToProtocol:@protocol(HasValue)])
    {
        return [((id <HasValue>) cell) setValue:value animate:YES];
    }
}

- (id)getValueForKey:(NSString*)key
{
    UITableViewCell* cell = [self cellWithKey:key];
    if (cell!=nil && [cell conformsToProtocol:@protocol(HasValue)])
    {
        return [((id <HasValue>) cell) getValue];
    }
    return nil;
}


- (CGFloat)calcPreferredAlignment
{
    // Calculate max preferred alignment
    CGFloat maxAlign = 0;
    for (UITableViewCell * cell in _cells)
    {
        if ([cell conformsToProtocol:@protocol(HasAlignment)])
        {
            id<HasAlignment> align = (id<HasAlignment>)cell;

            CGFloat preferred = [align calcPreferredAlignment];
            if (preferred > maxAlign)
                maxAlign = preferred;
        }
    }

    return maxAlign;
}

- (void)setActualAlignment:(CGFloat)alignment
{
    // Align cells
    for (UITableViewCell* cell in _cells)
    {
        if ([cell conformsToProtocol:@protocol(HasAlignment)])
        {
            id<HasAlignment> align = (id<HasAlignment>)cell;
            [align setActualAlignment:alignment];
        }
    }
}


@end