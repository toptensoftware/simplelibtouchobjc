//
// Created by Brad Robinson on 30/05/13.


#import "SimplePickerViewCell.h"


@implementation SimplePickerViewCell
{
    NSArray* _values;
    UIPickerView* _picker;
}

- (id)initWithLabel:(NSString*)labelText
{
    self = [super initWithLabel:labelText];
    if (self==nil)
        return nil;

    // Create picker
    _picker = [[UIPickerView alloc] init];
    _picker.delegate = self;
    _picker.dataSource = self;
    _picker.showsSelectionIndicator = YES;

    self.editor.inputView = _picker;
    self.editor.clearButtonMode = UITextFieldViewModeNever;

    return self;
}

- (id)initWithLabel:(NSString*)labelText andValues:(NSArray*)values andSelectedIndex:(int)index;
{
    self = [self initWithLabel:labelText];
    if (self==nil)
        return nil;

    // Store values
    _values = values;

    [self setSelectedIndex:index];

    // Done
    return self;
}


- (void)setValues:(NSArray*)values
{
    _values = values;
    [_picker reloadAllComponents];
}

- (void)setSelectedIndex:(long)index animated:(BOOL)animated
{
    if (index>=0)
    {
        self.editor.text = (NSString*)[_values objectAtIndex:index];
        [_picker selectRow:index inComponent:0 animated:animated];
    }
}

- (void)setSelectedValue:(NSString*)value animated:(BOOL)animated
{
    unsigned long index = [_values indexOfObject:value];
    if (index!=NSNotFound)
    {
        [self setSelectedIndex:index animated:animated];
    }
}

- (NSString*)pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return (NSString*) [_values objectAtIndex:row];
}

- (NSInteger)pickerView:(UIPickerView*)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _values.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView
{
    return 1;
}

- (void)pickerView:(UIPickerView*)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    super.TextValue = (NSString*)[_values objectAtIndex:row];

    if (self.ChangedBlock)
    {
        self.ChangedBlock(self.selectedIndex);
    }
}

- (long)selectedIndex
{
    return [_picker selectedRowInComponent:0];
}

- (void)setSelectedIndex:(long)index
{
    [self setSelectedIndex:index animated:NO];
}



@end