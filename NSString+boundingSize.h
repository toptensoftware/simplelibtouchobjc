//
//  NSString+boundingSize.h
//  SimpleLibTouch
//
//  Created by Brad Robinson on 16/10/13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (boundingSize)

- (CGSize)boundingSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode;
- (CGSize)boundingSizeWithFont:(UIFont *)font;

@end
