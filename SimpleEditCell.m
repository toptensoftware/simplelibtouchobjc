//
//  SimpleEditCell.m
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleEditCell.h"
#import "SimpleAppearance.h"
#import "SimpleKeyboardNavBar.h"

@interface SimpleEditCell()
{
    UILabel* _hintLabel;
}
@end;

@implementation SimpleEditCell

@synthesize showKeyboardNavBar;

- (void)setupAppearance
{
    // Setup appearance
    SimpleAppearance* a = [SimpleAppearance Instance];
    if (a!=nil)
    {
        self.textLabel.font = a.labelFont;
        self.textLabel.textColor = a.labelColor;
        m_editor.font = a.valueFont;
        m_editor.textColor = a.valueColor;
    }
}

- (id)initWithLabel:(NSString*)labelText
{
	return [self initWithLabel:labelText andText:@""];
}

- (id)initWithLabel:(NSString*)labelText andText:(NSString*)text
{
    return [self initWithLabel:labelText andText:text andPlaceholder:nil];
}


- (id)initWithLabel:(NSString*)labelText andText:(NSString*)text andPlaceholder:(NSString*)placeholder
{
	if (!(self=[super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil]))
		return nil;
	
	self.textLabel.text=labelText;
	

	// Create the editor
	m_editor=[[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, self.contentView.bounds.size.height) ];
	m_editor.delegate=self;
	m_editor.clearButtonMode=UITextFieldViewModeWhileEditing;
	m_editor.font=[UIFont systemFontOfSize:17];
	m_editor.text=text;
    m_editor.placeholder = placeholder;
    m_editor.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;

    [m_editor addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

	[self.contentView addSubview:m_editor];
	
	self.selectionStyle=UITableViewCellSelectionStyleNone;

    self.showKeyboardNavBar = YES;

	m_alignment=180;

    [self setupAppearance];

    return self;
}

- (void)textFieldDidChange:(UITextField*)field
{
    if (self.ChangeTextBlock!=nil)
    {
        self.ChangeTextBlock(field.text);
    }
}

- (id)initWithPlaceholder:(NSString*)placeholder andText:(NSString*)text
{
	if (!(self=[super init]))
		return nil;
	
	// Create the editor
	m_editor=[[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, self.contentView.bounds.size.height) ];
	m_editor.placeholder=placeholder;
	m_editor.delegate=self;
	m_editor.clearButtonMode=UITextFieldViewModeWhileEditing;
	m_editor.font=[UIFont systemFontOfSize:17];
	m_editor.text=text;
    m_editor.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;

	[self.contentView addSubview:m_editor];
	
	self.selectionStyle=UITableViewCellSelectionStyleNone;

    self.showKeyboardNavBar = YES;

    /*
	// This is a hack to force the UITableViewCell to correctly setup it's content view rectangle.
	// Without this the context rect of a default sized cell (44px height), doesn't get sized correctly
	// making it too wide and therefore the clear button on the edit field is wrong
	self.frame=CGRectMake(0, 0, 300, 10);
     */
	
	m_alignment=10;

    [self setupAppearance];

	return self;
}

- (id)initWithPlaceholder:(NSString*)placeholder
{
	return [self initWithPlaceholder:placeholder andText:@""];
}



- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.textLabel sizeToFit];
    self.textLabel.frame = CGRectMake(15,10,self.textLabel.frame.size.width, self.textLabel.frame.size.height);

    m_editor.frame = CGRectMake(
            m_alignment,
            10,
            self.contentView.frame.size.width-m_alignment,
            24
            );
    self.editor.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;



    if (_hintLabel!=nil)
    {
        _hintLabel.frame = CGRectMake(
                15,
                44 - 10,
                self.contentView.frame.size.width - 30,
                self.contentView.frame.size.height - 44);

    }
}

- (void)setValue:(id)value animate:(BOOL)animate
{
	if ([value isKindOfClass:[NSString class]])
		m_editor.text=(NSString*)value;
}

- (id)getValue
{
	return m_editor.text;
}

- (BOOL)didSelect:(UIViewController*)viewController
{
	return YES;
}

- (BOOL)willSelect
{
	[m_editor becomeFirstResponder];
	return FALSE;
}

- (void)takeFirstResponder
{
	[m_editor becomeFirstResponder];
    [self scrollVisible:YES];
}

- (BOOL)canBecomeFirstResponder
{
	return YES;
}

- (CGFloat)measureHeightForWidth:(CGFloat)width
{
    CGFloat height = 44;
    if (_hintLabel!=nil)
    {
        CGSize size = [_hintLabel sizeThatFits:CGSizeMake(width-30, 20000)];
        height += size.height + 10;
    }

    return height;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	SimpleTableViewController* viewController=(SimpleTableViewController*)[self.superview nextResponder];
    if (![viewController isKindOfClass:[SimpleTableViewController class]])
		return NO;	
	
	[viewController moveToNextFirstResponder:self];
	return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
	return YES;
}



- (UITextField*)editor
{
	return m_editor;
}

- (CGFloat)calcPreferredAlignment
{
    if (self.textLabel.text.length == 0)
        return 0;
    [self.textLabel sizeToFit];
    return self.textLabel.frame.size.width + 20;
}

- (void)setActualAlignment:(CGFloat)alignment
{
    if (self.textLabel.text.length == 0)
    {
        m_alignment = 10;
        return;
    }

    if (m_alignment!=alignment)
    {
        m_alignment = alignment;
        [self setNeedsLayout];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.showKeyboardNavBar && self.Controller!=nil)
    {
        SimpleKeyboardNavBar* navBar = self.Controller.keyboardNavBar;
        m_editor.inputAccessoryView = navBar;

    }

    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.showKeyboardNavBar && self.Controller!=nil)
    {
        SimpleKeyboardNavBar* navBar = self.Controller.keyboardNavBar;
        BOOL haveNext = [self.Controller findNextResponder:self]!=nil;
        BOOL havePrev = [self.Controller findPrevResponder:self]!=nil;
        [navBar enableNext:haveNext];
        [navBar enablePrev:havePrev];
    }

}

- (NSString*)TextValue
{
    return m_editor.text;
}

- (void)setTextValue:(NSString *)value
{
    m_editor.text = value;
}

- (NSString*)HintText
{
    return _hintLabel.text;
}

- (void)setHintText:(NSString*)value
{
    if (_hintLabel==nil)
    {
        _hintLabel = [[UILabel alloc] init];
        _hintLabel.backgroundColor = [UIColor clearColor];
        _hintLabel.numberOfLines = 0;
        _hintLabel.adjustsFontSizeToFitWidth = NO;
        _hintLabel.lineBreakMode = NSLineBreakByWordWrapping;

        SimpleAppearance* a = [SimpleAppearance Instance];
        if (a!=nil)
        {
            _hintLabel.font = a.hintFont;
            _hintLabel.textColor = a.hintColor;
        }

        [self.contentView addSubview:_hintLabel];
    }

    _hintLabel.text = value;

    [self setNeedsLayout];
}

@end
