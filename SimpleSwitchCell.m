//
//  SimpleSwitchCell.m
//
//  Created by Brad Robinson on 23/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import "SimpleSwitchCell.h"
#import "SimpleAppearance.h"


@implementation SimpleSwitchCell

- (void)setupAppearance
{
    // Setup appearance
    SimpleAppearance* a = [SimpleAppearance Instance];
    if (a!=nil)
    {
        self.textLabel.font = a.labelFont;
        self.textLabel.textColor = a.labelColor;
    }
}


- (id)initWithText:(NSString*)text
{
	if (!(self=[super init]))
		return nil;
	
	// Create the editor
	m_switch=[UISwitch new];
    [m_switch addTarget:self action:@selector(onChanged) forControlEvents:UIControlEventValueChanged];

    [self.contentView addSubview:m_switch];
	
	self.textLabel.textColor=[[UIColor new] initWithRed:0.25 green:.38 blue:.52 alpha:1];
	self.textLabel.text=text;
	self.textLabel.opaque=NO;
	self.textLabel.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0];
	self.selectionStyle=UITableViewCellSelectionStyleNone;

    [self setupAppearance];

	return self;
}

- (void)onChanged
{
    if (self.ChangeBlock!=nil)
    {
        self.ChangeBlock(m_switch.isOn);
    }
}

- (id)initWithText:(NSString*)text andValue:(BOOL)isOn
{
	if (!(self=[self initWithText:text]))
		return nil;

	[m_switch setOn:isOn animated:NO];
	
	return self;
}



- (void)setFrame:(CGRect)r
{
	[super setFrame:r];

    [m_switch sizeToFit];
    CGSize size=m_switch.frame.size;
	m_switch.frame=CGRectMake(self.contentView.frame.origin.x + self.contentView.frame.size.width - size.width-20, 
							  (self.contentView.frame.size.height-size.height)/2, 
							  size.width, size.height);
}

- (void)setValue:(id)value animate:(BOOL)animate
{
	if ([value isKindOfClass:[NSNumber class]])
	{
		[m_switch setOn:[(NSNumber*)value boolValue] animated:NO];
	}
}

- (id)getValue
{
	return [NSNumber numberWithBool:m_switch.on];
}

- (BOOL)didSelect:(UIViewController*)viewController
{
	return YES;
}

- (BOOL)willSelect
{
	return FALSE;
}

- (BOOL)shouldResignFirstResponder
{
	return TRUE;
}

- (UISwitch*)switchControl
{
    return m_switch;
}

@end
