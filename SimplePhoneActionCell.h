//
//  SimplePhoneActionCell.h
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleTableViewCell.h"
#import "SimpleTableViewController.h"

@interface SimplePhoneActionCell : SimpleTableViewCell<SelectHandler>
{
	NSString*		m_phoneNumber;
}

- (id)initWithPhoneNumber:(NSString*)number;

@end
