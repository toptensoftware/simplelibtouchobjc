//
//  SimpleDateTimeCell.h
//
//  Created by Brad Robinson on 22/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimpleTableViewController.h"
#import "SimpleTableViewCell.h"

@interface SimpleDateTimeCell : SimpleTableViewCell<HasValue, SelectHandler, UITextFieldDelegate, FirstResponder, MeasureHeight> 
{
	UIDatePicker*  	m_datePicker;
	UITableView*	m_parentTableView;
	NSString*		m_format;
}

typedef enum DateTimeType
{
	timeType,
	dateType,
	dateTimeType
} DateTimeType;


- (id)initWithLabel:(NSString *)label andDate:(NSDate*)date andType:(DateTimeType)type andParentView:(UITableView*)view;
- (void) updateDate;

@end
