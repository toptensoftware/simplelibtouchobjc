//
//  SimpleSwitchCell.h
//
//  Created by Brad Robinson on 23/04/10.
//  Copyright 2010 Topten Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimpleTableViewCell.h"
#import "SimpleTableViewController.h"

@interface SimpleSwitchCell : SimpleTableViewCell<HasValue, SelectHandler> {

	UISwitch*	m_switch;

}

- (id)initWithText:(NSString*)text;
- (id)initWithText:(NSString*)text andValue:(BOOL)isOn;

- (UISwitch*)switchControl;

@property (nonatomic, copy) void (^ChangeBlock)(BOOL);

@end
